#!/bin/bash
for i in {1..50000}
do
    echo "Welcome $i times"
    ./mvnw clean test

  if [ $? -eq 0 ]
    then
	echo "Successfully executed test"
   else
       echo "Some test has errors" >&2
       exit 1
  fi
done
